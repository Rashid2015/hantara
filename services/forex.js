var socketForex = require('socket.io-client')('http://bilbo.s.naxdev.com:8087');

exports.setTrader = function (data) {
    return new Promise((resolve, reject) => {
        let response = false;
        let extId = Math.random().toString(36).substr(2, 8);
        socketForex.emit('forex.user.set', {
            currency: data.currency || 'USD',
            leverage: data.leverage || 1,
            balance: data.balance || 0,
            margin: 0,
            pl: 0,
            extId: extId,
            soLevel: data.soLevel || 0.3,
            mcLevel: data.mcLevel || 1,
            id: data.id_trade || undefined // для обнавления его баланса ставить один раз при регистрации
        });
        socketForex.once('forex.user.set.response', (res) => {
            response = true;
            if (res.result != 1)
                reject({
                    status: 'STATUS_RESPONSE_INTERNAL_SERVER',
                    error: {code: 'RESPONSE_ERROR_FOREX', message: 'Can\'t do deposit'}
                });
            else
                resolve(res.data);
        });
        setTimeout(() => {
            if (!response)
                reject({
                    status: 'STATUS_RESPONSE_INTERNAL_SERVER',
                    error: {code: 'RESPONSE_SERVER_DOESNT_ANSWER', message: 'FOREX server doesn\'t answer'}
                });
        }, 5000);
    })
};

exports.openPosition = function (data) {
    return new Promise((resolve, reject)=> {
        let response = false;
        data.extId = Math.random().toString(36).substr(2, 8);
        socketForex.emit('forex.order.open', {
            userId: data.id,
            symbol: data.symbol || 'EURUSD',
            type: data.type || 1,
            priceSl: data.stop_loss || undefined,
            priceTp: data.take_profit || undefined,
            userCurrency: data.currency || 'USD',
            userLeverage: data.leverage || 1,
            volume: data.volume || 1,
            extId: data.extId,
            callbackClose: 'http://127.0.0.1:8082/positions',
            callbackOpen: 'http://127.0.0.1:8082/positions'
        });
        socketForex.once('forex.order.open.response', (res) => {
            response = true;
            if (res.result != 1)
                reject({
                    status: 'STATUS_RESPONSE_INTERNAL_SERVER',
                    error: {code: 'RESPONSE_ERROR_FOREX', message: res}
                });
            else
                resolve(res.data);
        });
        setTimeout(() => {
            if (!response)
                reject({
                    status: 'STATUS_RESPONSE_INTERNAL_SERVER',
                    error: {
                        code: 'RESPONSE_SERVER_DOESNT_ANSWER',
                        message: 'FOREX server with quotes doesn\'t answer'
                    }
                });
        }, 5000);
    })
};

exports.closePosition = function (data) {
    return new Promise((resolve, reject) => {
        let response = false;
        data.extId = Math.random().toString(36).substr(2, 8);
        socketForex.emit('forex.order.close', {
            id: data.id,
            extId: data.extId
        });

        socketForex.once('forex.order.close.response', (res)=> {
            response = true;
            if (res.result != 1)
                reject({
                    status: 'STATUS_RESPONSE_INTERNAL_SERVER',
                    error: {code: 'RESPONSE_ERROR_FOREX', message: 'Can\'t close position'}
                });
            else
                resolve(res.data);
        });
        setTimeout(() => {
            if (!response)
                reject({
                    status: 'STATUS_RESPONSE_INTERNAL_SERVER',
                    error: {code: 'RESPONSE_SERVER_DOESNT_ANSWER', message: 'FOREX server with quotes doesn\'t answer'}
                });
        }, 5000);
    })
};