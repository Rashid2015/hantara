var rds = require('../units/redis');
var forex = require('../services/forex');
var decorators = require('../units/decorators');

exports.open = function (socket, data) {
    if (data.data === null || typeof data.data != 'object') return;

    data.order = data.data;
    var order = {
        volume: +data.order.volume,
        type: data.order.type,
        currency: data.order.currency,
        symbol: data.order.symbol,
        open_price: data.order.open_price,
        take_profit: data.order.take_profit,
        stop_loss: data.order.stop_loss,
        comment: data.order.comment,
        status: 'pending'
    };

    rds.getAsync(`balance:${data.token.email}`)
        .then(result => {
            l(result);
            if (result === null) throw 'no balance';
            result = +result;
            if (result - order.volume < 0) throw 'not enough money';
            rds.set(`balance:${data.token.email}`, result - order.volume);
            return rds.incrAsync('counter:orders');
        })
        .then(result => {
            l(result);
            order.order_id = result;
            rds.sadd(`orders:${data.token.email}`, result);
            l(order);
            return rds.hmsetAsync(`order:${result}`, order);
        })
        .then(result => {
            l(result);
            return forex.setTrader(order);
        })
        .then(result => {
            l(result);
            return forex.openPosition(Object.assign(result, order));
        })
        .then(result => {
            l(result);
            var forex_order_id = result.id;
            rds.hmsetAsync(`forex_order:${forex_order_id}`, result);
            rds.set(`order_forex_to_hantara:${forex_order_id}`, order.order_id);
            rds.set(`order_hantara_to_forex:${order.order_id}`, forex_order_id);
            rds.hset(`order:${order.order_id}`, 'status', 'opened');
            order.status = 'opened';
            socket.emit('open order', order);
        })
        .catch(err => l(err));
};

exports.close = function (socket, data) {
    var order_id = data.order_id;
    var profit;

    rds.hgetAsync(`order:${order_id}`, 'status')
        .then(result => {
            l(result);
            if (result != 'opened') throw 'order must be opened';
            return rds.getAsync(`order_hantara_to_forex:${order_id}`);
        })
        .then(result => {
            l(result);
            return forex.closePosition({id: result});
        })
        .then(result => {
            l(result);
            profit = result.profit;
            return rds.getAsync(`balance:${data.token.email}`);
        })
        .then(result => {
            l(result);
            result = +result;
            rds.set(`balance:${data.token.email}`, result + profit);
            return rds.hsetAsync(`order:${order_id}`, 'status', 'closed');
        })
        .then(result => {
            l(result);
            socket.emit('close order', order_id);
        })
        .catch(err => l(err));
};

exports.getAll = function (socket, data) {
    rds.smembers(`orders:${data.token.email}`, (err, reply) => {
        if (err)
            socket.emit('get orders', {status: 'error', data: reply});

        else {
            var multi = rds.multi();

            reply.forEach(id => {
                multi.hgetall(`order:${id}`);
            });

            multi.exec((err, replies) => {
                var obj;

                if (err)
                    obj = {status: 'error', data: 'redis multi error'};

                else
                    obj = {
                        status: 'success', data: data.status ?
                            replies.filter(item => item.status == data.status) : replies
                    };

                if (data.status == 'closed')
                    socket.emit('get orders history', obj);
                else
                    socket.emit('get orders', obj);
            });
        }
    });
};

// навешиваем декораторы по принципу стека
// последний всегда сработает первым
for (var key in exports) {
    exports[key] = decorators.makeLogging(exports[key], key);
    exports[key] = decorators.tokenDecode(exports[key]);
    exports[key] = decorators.typeCheck(exports[key]);
}