var rds = require('../units/redis');
var jwt = require('../units/tokens');
var decorators = require('../units/decorators');

l('All users:');
rds.scan('user:*', keys => keys.forEach(key => rds.hgetall(key, (err, reply) => l(JSON.stringify(reply)))));

function issueToken(socket, data, update = false) {
    rds.hmget(`user:${data.email}`, 'password', 'firstname', 'lastname', (err, reply) => {
        var obj, password = reply[0],
            firstname = reply[1], lastname = reply[2],
            ip = socket.request.connection.remoteAddress;

        if (!password)
            obj = {status: 'error', data: 'user not found'};

        else if (password != data.password && !update)
            obj = {status: 'error', data: 'password mismatch'};

        else
            obj = {
                status: 'success',
                data: {
                    token: jwt.sign(data.email, ip,
                        data.strict_session === true ? socket.id : false),
                    firstname: firstname,
                    lastname: lastname
                }
            };

        socket.emit('login', obj);
    });
}

exports.login = function (socket, data) {
    if (data.token) {
        var decoded = jwt.decode(socket, data.token);

        if (decoded) {
            if (decoded.socket_id)
                data.strict_session = true;

            data.email = decoded.email;
            issueToken(socket, data, true);
        } else {
            socket.emit('login', {status: 'error', data: 'invalid token'});
        }
    } else {
        issueToken(socket, data);
    }
};

exports.signup = function (socket, data) {
    var user = {
        email: data.email,
        password: data.password,
        firstname: data.firstname,
        lastname: data.lastname,
        phone: data.phone,
        leverage: data.leverage,
        account_type: data.account_type
    };

    rds.scan(`user:${user.email}`, arr => {
        if (arr.length)
            socket.emit('signup', {status: 'error', data: 'user already exist'});
        else {
            rds.hmset(`user:${user.email}`, user, (err, reply) => {
                if (err) l(err);
                //if (reply) rds.publish('change_data', data);
                socket.emit('signup', {status: 'success', data: reply});
            });
            rds.set(`balance:${user.email}`, 1000);
        }
    });
};

exports.getBalance = function (socket, data) {
    rds.get(`balance:${data.token.email}`, (err, reply) => {
        var obj;

        if (err)
            obj = {status: 'error', data: reply};

        else
            obj = {status: 'success', data: reply};

        socket.emit('get balance', obj);
    });
};

exports.subscribeQuotes = function (socket, data) {
    socket.join('quotes');
};

// навешиваем декораторы по принципу стека
// последний всегда сработает первым
for (var key in exports) {
    exports[key] = decorators.makeLogging(exports[key], key);
    if (~['getBalance', 'subscribeQuotes'].indexOf(key)) {
        exports[key] = decorators.tokenDecode(exports[key]);
    }
    exports[key] = decorators.typeCheck(exports[key]);
}