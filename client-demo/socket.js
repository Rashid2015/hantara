// var socket = io('http://localhost:8086', {autoConnect: false});
// var socket = io('http://52.57.110.106:8086', {autoConnect: false});
var socket = io('http://52.57.232.76:8086', {autoConnect: false});

var token, last_order_id;

socket.on('token almost expired', function (data) {
    console.log('token almost expired', data);
    if (data.status == 'success') {
        // get new token...
    }
});

socket.on('get balance', function (data) {
    console.log('get balance', data);
    if (data.status == 'success') {
        // show balance...
    }
});

socket.on('open order', function (data) {
    console.log('open order', data);
    last_order_id = data.order_id;
});

socket.on('close order', function (data) {
    console.log('close order', data);
});

socket.on('get orders', function (data) {
    console.log('get orders', data);
    if (data.status == 'success') {
        // user orders...
    }
});

socket.on('login', function (data) {
    console.log('login', data);
    if (data.status == 'success') {
        // logged in...
        token = data.data.token;
    }
});

socket.on('signup', function (data) {
    console.log('signup', data);
    if (data.status == 'success') {
        // signed up...
    }
});

socket.on('quote update', function (data) {
    console.log('quote update', data);
});

socket.on('disconnect', function () {
    console.log('disconnect');
});

socket.on('connect', function () {
    console.log('connect');
    //clientLogin();
});

socket.connect();