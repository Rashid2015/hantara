/**
 * JSON Web Token (JWT)
 */

const SECRET = 'f4O%e5|8wo$|iGjTOfB~RT6AC';

var jwt = require('jsonwebtoken');

function signJWT(email, ip, socket_id) {
    var obj = {
        email: email,
        ip: ip
    };

    if (socket_id)
        obj.socket_id = socket_id;

    return jwt.sign(obj, SECRET, {expiresIn: '1d'});
}

function decodeJWT(socket, token) {
    try {
        var decoded = jwt.verify(token, SECRET);

        // проверка IP
        if (decoded.ip != socket.request.connection.remoteAddress)
            throw 'Error: id address mismatch';

        // проверка socket.id для строгих сессий
        if (decoded.socket_id && decoded.socket_id != socket.id)
            throw 'Error: socket.id mismatch';

        // обновление токена за 3 часа до окончания срока действия
        if (decoded.exp - Math.floor(Date.now() / 1000) < 10800) {
            var obj = {
                token: jwt.sign(decoded.email, decoded.ip,
                    decoded.socket_id ? decoded.socket_id : false)
            };
            socket.server.to(socket.id).emit('token almost expired', obj);
        }

        return decoded;
    } catch (err) {
        l(err);
        return false;
    }
}

module.exports = {
    sign: signJWT,
    decode: decodeJWT
};