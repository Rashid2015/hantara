/**
 * Financial quotes (котировки)
 */

var symbols = {
    'EURUSD': 5,
    'GLD': 3,
    'ADS': 3,
    'AMZN': 3,
    'AAPL': 3,
    'BAC': 3,
    'KO': 3,
    'FB': 3,
    'GE': 3,
    'GOOGL': 3,
    'MSFT': 3,
    'NKE': 3,
    'OIL': 3,
    'TSLA': 3,
    'GBPUSD': 5,
    'USDCHF': 5,
    'USDJPY': 5,
    'EURJPY': 5,
    'EURGBP': 5
};

var socketQuotes = require('socket.io-client')('http://bilbo.s.naxdev.com:8081');

module.exports = function (io) {
    socketQuotes.on('connect', function () {
        Object.keys(symbols).forEach(symbol => {
            socketQuotes.emit('quotes.sub', {symbol: symbol, frame: 1});
            socketQuotes.on('quotes.' + symbol + '.1', data => {
                io.to('quotes').emit('quote update',
                    {symbol: symbol, bid: data[0], ask: data[1]});
            });
        })
    })
};