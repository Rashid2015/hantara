/**
 * Decorators
 */

var jwt = require('./tokens');

// логирующий декоратор
exports.makeLogging = function (f, name) {
    var counter = 0;

    return function () {
        l(`${name} (${++counter})`);

        return f.apply(this, arguments);
    }
};

// для проверки типа
exports.typeCheck = function (f) {
    return function (socket, data) {
        if (data !== null && typeof data == 'object')
            return f.call(this, socket, data);

        l('Error: object required');
    }
};

// для проверки и расшифровки токена
exports.tokenDecode = function (f) {
    return function (socket, data) {
        var decoded = jwt.decode(socket, data.token);

        if (decoded) {
            data.token = decoded;
            return f.call(this, socket, data);
        }

        l('Error: invalid token');
    }
};