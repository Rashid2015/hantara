/**
 * REDIS init routines
 */

const REDIS_SELECT_DB = 2;

var redis = require('redis');

// промисифицируем REDIS (добавляем client.*Async обертки)
var bluebird = require('bluebird');
bluebird.promisifyAll(redis.RedisClient.prototype);

// создаем клиента и подключаемся к базе
var rds = redis.createClient();
rds.select(REDIS_SELECT_DB, err => err ? l('Redis error: ' + err) : 'ok');
rds.on('error', err => l('Redis error: ' + err));

// переопределяем функцию SCAN для упрощения поиска ключей
var parentScan = rds.scan.bind(rds);
rds.scan = function (pattern, cb) {
    var args = ['match', pattern, 'count', '100', handler],
        result = [];

    function handler(err, reply) {
        if (err) throw err;

        result.push(...reply[1]);

        if (reply[0] != '0')
            parentScan(reply[0], ...args);
        else
            cb(result);
    }

    parentScan('0', ...args);
};

module.exports = rds;