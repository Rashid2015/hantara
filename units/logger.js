/**
 * Simple log technique
 */

var fs = require('fs');
var util = require('util');

var path = require('path');
var appDir = path.dirname(require.main.filename);

var filename, stream;

function l(data) {
    console.log(data);
    var date = new Date().toISOString();

    try {
        var name = appDir + '/logs/log-' + date.substring(0, 10) + '.log';

        if (filename != name) {
            if (stream != undefined)
                stream.end('\n');

            stream = fs.createWriteStream(filename = name, {'flags': 'a'});
        }

        stream.write('[' + date.substring(11, 23) + '] ' + util.inspect(data) + '\n');
    }
    catch (err) {
        console.log('LOG ERROR: ' + err);
    }
}

module.exports = l;