/**
 * Project Hantara
 *
 * Copyright 2016
 */

const V_HANTARA = 'v0.041';

// глобальный логгер
global.l = require('./units/logger');

l('Hantara ' + V_HANTARA + '\n' + 'Start...');

var users = require('./models/users');
var orders = require('./models/orders');

// объект Socket.IO
var io = require('socket.io')();

// назначаем сокетам обработчики событий
io.on('connection', function (socket) {
    l('connection');

    socket.on('disconnect', function () {
        l('disconnect');
    });

    socket.on('login', function (data) {
        users.login(this, data);
    });

    socket.on('signup', function (data) {
        users.signup(this, data);
    });

    socket.on('subscribe quotes', function (data) {
        users.subscribeQuotes(this, data);
    });

    socket.on('get balance', function (data) {
        users.getBalance(this, data);
    });

    socket.on('open order', function (data) {
        orders.open(this, data);
    });

    socket.on('close order', function (data) {
        orders.close(this, data);
    });

    socket.on('get orders', function (data) {
        orders.getAll(this, data);
    });
});

// запуск Socket.IO сервера
io.listen(8086);

// подписка на котировки
require('./units/quotes')(io);